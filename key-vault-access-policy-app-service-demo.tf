resource "azurerm_key_vault_access_policy" "kvap-demo" {
  key_vault_id = azurerm_key_vault.kv.id

  tenant_id = azurerm_app_service.demo.identity.0.tenant_id
  object_id = azurerm_app_service.demo.identity.0.principal_id

  secret_permissions = [
    "get", "list",
  ]
}

