resource "azurerm_key_vault_access_policy" "kvap-service-principal" {
  key_vault_id = azurerm_key_vault.kv.id

  tenant_id = data.azurerm_client_config.current.tenant_id
  object_id = data.azurerm_client_config.current.object_id

  secret_permissions = [
    "get", "set", "delete", "list",
  ]
}
