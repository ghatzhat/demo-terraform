#!/bin/bash

# set each of these values for an Azure Service Principal, and run this file on the command line
export TF_VAR_subscription_id=
export TF_VAR_client_id=
export TF_VAR_client_secret=
export TF_VAR_tenant_id=
