resource "azurerm_resource_group" "rg" {
  name = format("%s-%s", var.project, var.environment)
  location = var.location
}
