resource "azurerm_app_service" "demo" {
  name = format("as-%s-%s-demo", var.project, var.environment)
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  app_service_plan_id = azurerm_app_service_plan.asp.id
  https_only = true

  app_settings = {
    SCM_DO_BUILD_DURING_DEPLOYMENT = true
    STORAGE_ACCOUNT_KEY = "@Microsoft.KeyVault(SecretUri=${azurerm_key_vault_secret.kvs-storage-account-key.id})"
    STORAGE_ACCOUNT_NAME = "@Microsoft.KeyVault(SecretUri=${azurerm_key_vault_secret.kvs-storage-account-name.id})"
  }

  site_config {
    always_on = true
  }

  identity {
    type = "SystemAssigned"
  }

  auth_settings {
    enabled = false
    token_store_enabled = true
  }
}
