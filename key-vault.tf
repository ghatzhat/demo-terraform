resource "azurerm_key_vault" "kv" {
  name = format("kv-%s-%s", var.project, var.environment)
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  enabled_for_disk_encryption = true
  tenant_id = data.azurerm_client_config.current.tenant_id
  sku_name = "standard"

  network_acls {
    default_action = "Allow"
    bypass = "AzureServices"
  }
}
