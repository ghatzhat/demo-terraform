provider "azurerm" {
  version = "=1.43.0"
  # these variables need to come from environment - see env.sh for one way to do this
  # - subscription_id
  # - client_id
  # - client_secret
  # - tenant_id
}
