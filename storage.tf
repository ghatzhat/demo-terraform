resource "azurerm_storage_account" "sa" {
  name = replace(format("%s%s%s", "sa", var.project, var.environment), "-", "")
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  account_kind = "StorageV2"
  account_tier = "Standard"
  account_replication_type = "GRS"
}
