resource "azurerm_key_vault_secret" "kvs-storage-account-name" {
  key_vault_id = azurerm_key_vault.kv.id
  name = "storage-account-name"
  value = azurerm_storage_account.sa.name
  depends_on = [azurerm_key_vault_access_policy.kvap-service-principal]
}

resource "azurerm_key_vault_secret" "kvs-storage-account-key" {
  key_vault_id = azurerm_key_vault.kv.id
  name = "storage-account-key"
  value = azurerm_storage_account.sa.primary_access_key
  depends_on = [azurerm_key_vault_access_policy.kvap-service-principal]
}
