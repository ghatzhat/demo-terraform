resource "azurerm_app_service_plan" "asp" {
  name = format("asp-%s-%s", var.project, var.environment)
  resource_group_name = azurerm_resource_group.rg.name
  location = azurerm_resource_group.rg.location
  kind = "Linux"
  reserved = true

  sku {
    tier = "Standard"
    size = "S1"
  }
}
