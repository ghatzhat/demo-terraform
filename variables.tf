variable "environment" {
  default = "dev"
}

variable "location" {
  default = "uksouth"
}

variable "project" {
  default = "demo-terraform"
}
